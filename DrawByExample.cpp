// DrawByExample.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <opencv2/opencv.hpp>
#include <opencv\highgui.h>
using namespace cv;
using namespace std;

int mainxxx()
{
	// Create black empty images
	Mat image = Mat::zeros(400, 400, CV_8UC3);

	// Draw a line 
	line(image, Point(15, 20), Point(70, 50), Scalar(110, 220, 0), 2, 8);

	// Draw a circle 
	circle(image, Point(200, 200), 32.0, Scalar(0, 0, 255), 1, 8);

	// Draw a ellipse 
	ellipse(image, Point(200, 200), Size(100.0, 160.0), 45, 0, 360, Scalar(255, 0, 0), 1, 8);
	ellipse(image, Point(200, 200), Size(100.0, 160.0), 135, 0, 360, Scalar(255, 0, 0), 10, 8);
	ellipse(image, Point(200, 200), Size(150.0, 50.0), 135, 0, 360, Scalar(0, 255, 0), 1, 8);

	// Draw a rectangle ( 5th argument is not -ve)
	rectangle(image, Point(15, 20), Point(70, 50), Scalar(0, 55, 255), +1, 4);

	// Draw a filled rectangle ( 5th argument is -ve)
	rectangle(image, Point(115, 120), Point(170, 150), Scalar(100, 155, 25), -1, 8);
	
	int w = 400;
	// Draw a circle 
	/** Create some points */
	Point rook_points[1][20];
	rook_points[0][0] = Point(w / 4.0, 7 * w / 8.0);
	rook_points[0][1] = Point(3 * w / 4.0, 7 * w / 8.0);
	rook_points[0][2] = Point(3 * w / 4.0, 13 * w / 16.0);
	rook_points[0][3] = Point(11 * w / 16.0, 13 * w / 16.0);
	rook_points[0][4] = Point(19 * w / 32.0, 3 * w / 8.0);
	rook_points[0][5] = Point(3 * w / 4.0, 3 * w / 8.0);
	rook_points[0][6] = Point(3 * w / 4.0, w / 8.0);
	rook_points[0][7] = Point(26 * w / 40.0, w / 8.0);
	rook_points[0][8] = Point(26 * w / 40.0, w / 4.0);
	rook_points[0][9] = Point(22 * w / 40.0, w / 4.0);
	rook_points[0][10] = Point(22 * w / 40.0, w / 8.0);
	rook_points[0][11] = Point(18 * w / 40.0, w / 8.0);
	rook_points[0][12] = Point(18 * w / 40.0, w / 4.0);
	rook_points[0][13] = Point(14 * w / 40.0, w / 4.0);
	rook_points[0][14] = Point(14 * w / 40.0, w / 8.0);
	rook_points[0][15] = Point(w / 4.0, w / 8.0);
	rook_points[0][16] = Point(w / 4.0, 3 * w / 8.0);
	rook_points[0][17] = Point(13 * w / 32.0, 3 * w / 8.0);
	rook_points[0][18] = Point(5 * w / 16.0, 13 * w / 16.0);
	rook_points[0][19] = Point(w / 4.0, 13 * w / 16.0);

	const Point* ppt[1] = { rook_points[0] };
	int npt[] = { 20 };

	fillPoly(image, ppt, npt, 1, Scalar(255, 255, 255), 8);

	// Draw a text
	putText(image, "Hi all...", Point(50, 100), FONT_HERSHEY_SIMPLEX, 1, Scalar(0, 200, 200), 4);
	
	imshow("Image", image);

	waitKey(0);
	return(0);
}
